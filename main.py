#!/usr/bin/env python
# coding: utf-8

## In The Name of Allah

import argparse
import json
import logging
import pathlib as path

from typing import List, Union

import nltk
import hazm


def main():
    args = argparse.ArgumentParser()
    args.add_argument("--input", "-i", type=str, required=True)
    args.add_argument("--output", "-o", type=str, required=False, default="output.txt")
    args.add_argument("--max-sentence-length", "-m", type=int, required=False, default=6)
    args.add_argument("--log", "-l", type=str, required=False, default="INFO",
                        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"])
    args.add_argument("--prepare-nltk", "-p", type=bool, required=False, default=False)
    LOG_LEVELS = {
        "DEBUG": logging.DEBUG,
        "INFO": logging.INFO,
        "WARNING": logging.WARNING,
        "ERROR": logging.ERROR,
        "CRITICAL": logging.CRITICAL
    }
    args = args.parse_args()
    logging.basicConfig(level=LOG_LEVELS[args.log])
    
    logging.debug(f"prepare_nltk: {args.prepare_nltk}")
    if args.prepare_nltk:
        logging.info(f"Preparing nltk")
        nltk.download("all")
    else:
        logging.info(f"Skipping nltk preparation")
    
    logging.info(f"Input file: {args.input}")
    logging.debug(f"Calling parse_input()")
    logging.info(f"Output file: {args.output}")
    texts = parse_input(args.input)
    
    logging.debug(f"Calling segment_text()")
    segments = segment_text(texts, max_sentence_length=args.max_sentence_length)
    
    logging.debug(f"Calling write_output()")
    write_output(args.output, segments)
    logging.info(f"Output written to {args.output}")

def parse_input(path: Union[str, path.Path]) -> List[str]:
    """Parse input JSON file into a list of texts.

    Args:
        path (str | path.Path): Path to Telegram chat JSON export file.
    """
    
    with open(path, "r", encoding="utf-8") as f:
        data = json.load(f)
    
    texts = []
    for message in data["messages"]:
        texts.append([])
        for text_part in message["text_entities"]:
            texts[-1].append(text_part["text"].strip())
        texts[-1] = " ".join(texts[-1])
    
    return texts

def segment_text(texts: List[str], *, max_sentence_length: int = 6) -> List[str]:
    """Normalize text and split into sentences.

    Args:
        texts (List[str]): List of texts.
        max_sentence_length (int, optional): Maximum number of words in each sentence. Defaults to 6.

    Returns:
        List[str]: List of sentences with max_sentence_length words each.
    """
    
    _normalizer = hazm.Normalizer()
    normalized_texts = [_normalizer.normalize(text) for text in texts]
    
    sentences = [sent for text in normalized_texts for sent in hazm.sent_tokenize(text)]
    
    return list(filter(
        lambda sent: 2 < len(hazm.word_tokenize(sent)) <= max_sentence_length + 2,
        sentences))

def write_output(path: Union[str, path.Path], segments: List[str]) -> None:
    """Write segments to output file.

    Args:
        path (Union[str, path.Path]): Path to output file.
        segments (List[str]): List of segments.
    """
    
    with open(path, "w", encoding="utf-8") as f:
        for segment in segments:
            f.write(segment + "\n")

if __name__ == "__main__":
    main()